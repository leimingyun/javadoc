# 一分钟放到项目中使用
### 1. pom.xml 中增加依赖

````
<!-- javadoc https://gitee.com/leimingyun/javadoc -->
<dependency>
  <groupId>com.xnx3.doc.javadoc</groupId>
  <artifactId>javadoc</artifactId>
  <version>1.16</version>
  <scope>compile</scope>
</dependency>
````

### 2. 一行代码导出
新建一个类,代码如下，直接运行，即可生成文档

````
package com;

/**
 * 自动扫描指定的包下所有controller的json接口，根据其标准的JAVADOC注释，生成接口文档。  
 * 使用参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=4518712&doc_id=1101390
 * @author 管雷鸣
 */
public class ApiDoc {
	public static void main(String[] args) {
		new com.xnx3.doc.JavaDoc("com.xnx3.demo").generateHtmlDoc();
	}
}
````

# 注释的写法
![javadoc_example.png](https://images.gitee.com/uploads/images/2021/1213/144029_382493c6_429922.png)

### Controller 类的注意事项

````
/**
 * 用户相关
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/demo/simple/")
public class SimpleController{
	
}
````

* 要有 ``@RequestMapping``  注解，才会被扫描到

### 方法的注释及传入参数

````
/**
 * 传入姓名跟年龄，进行保存
 * @author 管雷鸣
 * @param name 姓名 
 * @param age 年龄
 * @param token 当前操作用户的唯一标识，登录标识 <required> <example=ef68481a-a467-44ae-8c16-952d6f5f009b>
 * @return 执行的结果
 */
@RequestMapping(value="setPerson.json", method = RequestMethod.POST)
@ResponseBody
public BaseVO setPerson(
		@RequestParam(required = false, defaultValue="管雷鸣") String name,
		@RequestParam(required = true, defaultValue="28") int age){	
	// ...
	return BaseVO.success("操作成功");
}
````
* @ResponseBody、@RequestMapping(或 @PostMapping) 只有加了这两个注解，在生成文档时才会将此方法自动生成接口文档。
* 传入的参数中 @RequestParam 的标注，其中
  * required 生成的文档说明中，这个传入参数是否必传项
  * defaultValue 生成的文档说明中，这个传入参数的示例值
* 某个传如参数如果不需要有 @RequestParam ，那么不必为了应对文档而产生垃圾代码，可以直接在 javadoc 中的 @param 里使用指定几个标签进行标注
  * ```` <required> ````  标注当前参数是否是必传。如果没有此标签，此传参默认为非必传
  * ```` <type=int> ```` 标注当前参数要传入的数据类型，如 ```` <type=int> ````、 ````<type=float> ````、```` <type=string> ```` 等，如果没有此标签，生成文档中的传入类型默认为 string 类型
  * ```` <example=Java开发> ```` 标注此传入参数可传入值的示例，在生成文档中的传入示例这里显示，让观看文档者能更好知晓要传入什么

### 方法（接口）返回响应
##### Controller 的方法：

````
/**
 * 获取某个人的信息
 * @author 管雷鸣
 * @return 某个人的信息
 */
@RequestMapping(value="gainPerson.json", method = RequestMethod.POST)
@ResponseBody
public PersonVO gainPerson(){	
	PersonVO vo = new PersonVO();
	
	// ... 省略
	
	return vo;
}
````

##### PersonVO:

````
package com.xnx3.demo.vo;
import com.xnx3.BaseVO;
import com.xnx3.demo.entity.Person;

/**
 * 人员信息（用于json接口的返回值响应）
 * @author 管雷鸣
 */
public class PersonVO extends BaseVO{
	private Person person;	//人员信息

	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}

````

##### Person 实体类（或者 java bean 类）：

````
package com.xnx3.demo.entity;

/**
 * 人员表，演示的实体类。这里演示就不加那些实体类相关注解了
 * @author 管雷鸣
 */
public class Person{
	private String name; 	//人员姓名
	private Integer age;	//年龄，几岁
	
	// get、set 、 tostring ... 这里只是演示就省略不写了
}
````

VO类以及实体类中注释采用直接在后面跟上 // 注释的方式。  
注意，要在 private String xxx; //这后面跟上注释，不要放到上面。  
这里的注释会在接口自动生成返回值时，将返回值的每个字段代表什么意思列出来  

# 使用扩展
### 注释的美观程度
注释中可以使用html标签进行如换行等操作，美化输出的注释内容
### 多项目引用
如当前开发的项目，比如示例中的  BaseVO 是一个jar xnx3-util.jar （https://gitee.com/leimingyun/xnx3_util） 中的，在生成文档时 BaseVO 中的 result 、 info 是没有注释的，可以将xnx3-util项目也拉下来，比如你当前项目在 /mac/git/项目目录 ，那拉下来的 xnx3-util 也要在 /mac/git/ 目录下，这样生成文档时会自动扫描相关项目进行抽取注释。
这里面默认引入了 xnx3-util、 xnx3-weixin、wm、wangmarket、wangmarket_shop 等项目，如果你有其他jar包要引入，在生成文档的代码中可以这样增加 

````
doc.javaSourceFolderList.add("/Users/apple/git/page.java/");
````
传入的这个 page.java 便是你项目拉下来后的目录名，注意这个是目录，最后要有 /   
增加后完整的代码为

````
JavaDoc doc = new JavaDoc("com.xnx3.demo");
doc.javaSourceFolderList.add("/Users/apple/git/page.java/");
doc.generateHtmlDoc();
````

### 自定义文档模板
##### 原始文档模板下载
....

##### 模板中通用变量

* {name} 当前文档的名字
* {version} 当前文档的版本
* {domain} 当前文档默认请求的接口域名，这里输出如 http://api.zvo.cn 

以上三个变量在所有模板文件（index.html、template.html、style.css、javadoc.js）都可以直接使用。  
其变量的内容，需要再生成文档时在Java代码中进行设置：

````
JavaDoc doc = new com.xnx3.doc.JavaDoc("com.xnx3.wangmarket.shop.api.controller");
doc.templatePath = "/Users/apple/Downloads/javadoc/";		//本地模板所在磁盘的路径
doc.name = "网市场云商城-用户端-API文档";				//文档的名字
doc.domain = "http://api.zvo.cn";				//文档中默认的接口请求域名
doc.version = "1.7";						//当前做的软件系统的版本号

doc.generateHtmlDoc();	//生成文档
````

# 常见问题

### Eclipse中运行时，出现错误 java.lang.NoClassDefFoundError: com/sun/tools/javadoc/Main
常见于第一次在使用，eclipse用的默认自带的jre，运行时报错如下：
````
Exception in thread "main" java.lang.NoClassDefFoundError: com/sun/tools/javadoc/Main
	at com.xnx3.doc.javadoc.JavaDocReader.readDoc(JavaDocReader.java:119)
	at com.xnx3.doc.javadoc.JavaDocUtil.getJavaDoc(JavaDocUtil.java:49)
	at com.xnx3.doc.JavaDoc.searchController(JavaDoc.java:227)
	at com.xnx3.doc.JavaDoc.generateHtmlDoc(JavaDoc.java:84)
	at cn.ApiDoc.main(ApiDoc.java:18)
Caused by: java.lang.ClassNotFoundException: com.sun.tools.javadoc.Main
	at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:636)
	at java.base/jdk.internal.loader.ClassLoaders$AppClassLoader.loadClass(ClassLoaders.java:182)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:519)
	... 5 more
````

1. 下载JDK8安装到电脑上
2. 将jdk1.8.../lib/tools.jar 加入项目依赖，即可。

# 其他
### 项目简介
本项目最初为wm快速开发 (http://wm.zvo.cn) 中的文档自动生成模块，后独立出来，可用于任何 springmvc 的项目中进行使用，为接口生成接口文档。
### 完整代码DEMO示例
https://gitee.com/leimingyun/javadoc/tree/master/src/main/java/com/xnx3/demo