package com.xnx3.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.BaseVO;
import com.xnx3.demo.vo.PersonVO;

/**
 * 用户相关
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/demo/simple/")
public class SimpleController{
	
	/**
	 * 传入姓名跟年龄，进行保存
	 * @author 管雷鸣
	 * @param name 姓名 
	 * @param age 年龄
	 * @param token 当前操作用户的唯一标识，登录标识 <required> <example=ef68481a-a467-44ae-8c16-952d6f5f009b>
	 * @return 执行的结果
	 */
	@RequestMapping(value="setPerson.json", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO setPerson(
			@RequestParam(required = false, defaultValue="管雷鸣") String name,
			@RequestParam(required = true, defaultValue="28") int age){	
		// ...
		return BaseVO.success("操作成功");
	}
	
	/**
	 * 获取某个人的信息
	 * @author 管雷鸣
	 * @return 某个人的信息
	 */
	@RequestMapping(value="gainPerson.json", method = RequestMethod.POST)
	@ResponseBody
	public PersonVO gainPerson(){	
		PersonVO vo = new PersonVO();
		
		// ... 省略
		
		return vo;
	}
}
