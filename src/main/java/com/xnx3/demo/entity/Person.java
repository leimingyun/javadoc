package com.xnx3.demo.entity;

/**
 * 人员表，演示的实体类。这里演示就不加那些实体类相关注解了
 * @author 管雷鸣
 */
public class Person{
	private String name; 	//人员姓名
	private Integer age;	//年龄，几岁
	
	// get、set 、 tostring ... 这里只是演示就省略不写了
}