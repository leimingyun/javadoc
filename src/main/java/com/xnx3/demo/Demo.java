package com.xnx3.demo;

import com.xnx3.doc.JavaDoc;

/**
 * 演示Demo，直接右键运行即可生成文档
 * @author 管雷鸣
 *
 */
public class Demo {
	public static void main(String[] args) {
		JavaDoc doc = new com.xnx3.doc.JavaDoc("com.xnx3.demo");
		//doc.templatePath = "/Users/apple/Downloads/javadoc/";
		doc.version = "v1.2";
		doc.name = "网市场云商城用户端API";
		doc.welcome = "简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明简介说明";
		doc.generateHtmlDoc();
	}
}
