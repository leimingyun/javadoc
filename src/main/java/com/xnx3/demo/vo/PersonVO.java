package com.xnx3.demo.vo;

import com.xnx3.BaseVO;
import com.xnx3.demo.entity.Person;

/**
 * 人员信息（用于json接口的返回值响应）
 * @author 管雷鸣
 */
public class PersonVO extends BaseVO{
	private Person person;	//人员信息

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
}
