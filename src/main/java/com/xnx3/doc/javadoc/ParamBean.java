package com.xnx3.doc.javadoc;

/**
 * javadoc method 传参数相关
 * @author 管雷鸣
 *
 */
public class ParamBean {
	private String name;	//参数名
	private String commentText;	//注释内容
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
}
