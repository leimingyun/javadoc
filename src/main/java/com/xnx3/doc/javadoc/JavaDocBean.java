package com.xnx3.doc.javadoc;

import java.util.List;

/**
 * javadoc class 相关
 * @author 管雷鸣
 */
public class JavaDocBean {
	private String className;	//类名
	private String commentText;	//注释内容
	private String author;	//作者
	
	private List<JavaDocMethodBean> methodList;	//其内方法列表

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public List<JavaDocMethodBean> getMethodList() {
		return methodList;
	}

	public void setMethodList(List<JavaDocMethodBean> methodList) {
		this.methodList = methodList;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
}
