package com.xnx3.doc.javadoc;

import java.util.Map;

/**
 * javadoc method 方法相关
 * @author 管雷鸣
 *
 */
public class JavaDocMethodBean {
	private String methodName;	//方法名,如 getAllCity
	private String commentText;	//注释内容
	private String author;		//这个方法的作者
	private String returnCommentText;	//方法上面加的 @return 的文字描述
	private Map<String, ParamBean> params;	//方法传递的参数。key参数名，value参数说明
	
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Map<String, ParamBean> getParams() {
		return params;
	}
	public void setParams(Map<String, ParamBean> params) {
		this.params = params;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getReturnCommentText() {
		return returnCommentText;
	}
	public void setReturnCommentText(String returnCommentText) {
		this.returnCommentText = returnCommentText;
	}
	
}
