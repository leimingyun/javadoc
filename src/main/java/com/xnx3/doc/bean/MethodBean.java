package com.xnx3.doc.bean;

import java.util.HashMap;
import java.util.Map;

import com.xnx3.doc.TemplateUtil;

import net.sf.json.JSONObject;

/**
 * class类的方法
 * @author 管雷鸣
 *
 */
public class MethodBean{
	private String methodName;	//方法名,如 getAllCity
	private String methodType;	//请求类型，如 POST、GET
	private String commentText;	//注释内容
	private String urlFile;		//url的请求文件，如 list.json
	private String apiUrl;		//api请求的url，如 /shop/api/user/getUser.json
	private String author;	//作者
	private Map<String, ParamBean> params;	//方法传递的参数。key参数名，value参数说明
	private String returnVoClassName;	//返回的vo的class的name，其值如 com.xnx3.j2ee.vo.BaseVO
	private VOBean returnValue;	// 返回值，具体参数说明
	private String returnCommentText;	//方法上面加的 @return 的文字描述
	private boolean login;	//当前接口是否是登录接口。如果是登录接口，那么HTML中会在请求完成后，如果result=1，自动将返回的token进行保存
	
	public MethodBean() {
		this.params = new HashMap<String, ParamBean>();
	}
	
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		if(commentText.indexOf("<login>") > -1) {
			//当前接口是登录获取token的接口
			this.login = true;
		}
		
		this.commentText = commentText;
	}
	public String getUrlFile() {
		return urlFile;
	}
	public void setUrlFile(String urlFile) {
		this.urlFile = urlFile;
	}
	
	public Map<String, ParamBean> getParams() {
		return params;
	}
	public void setParams(Map<String, ParamBean> params) {
		this.params = params;
	}
	public String getReturnVoClassName() {
		return returnVoClassName;
	}
	public void setReturnVoClassName(String returnVoClassName) {
		this.returnVoClassName = returnVoClassName;
	}
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public VOBean getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(VOBean returnValue) {
		this.returnValue = returnValue;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		apiUrl = apiUrl.replaceAll("//", "/");
		this.apiUrl = apiUrl;
	}

	public String getReturnCommentText() {
		return returnCommentText;
	}

	public void setReturnCommentText(String returnCommentText) {
		this.returnCommentText = returnCommentText;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public boolean isLogin() {
		return login;
	}

	public void setLogin(boolean login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return JSONObject.fromObject(this).toString();
	}
}
