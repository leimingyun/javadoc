package com.xnx3.doc.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.xnx3.Log;
import com.xnx3.StringUtil;
import com.xnx3.doc.FilterUtil;
import com.xnx3.doc.javadoc.JavaDocBean;
import net.sf.json.JSONObject;

/**
 * api方法的某个传入参数的信息
 * @author 管雷鸣
 *
 */
public class ParamBean{
	private String name;	//参数名
	private String commentText;	//注释内容
	private String type;	//参数类型，取值有 string、int、Float 等
	private String defaultValue;	//默认值，接口文档中也会显示出来的默认值。这个是 RequestParam(name=, value=, defaultValue=潍坊市, required=false) 这个defaultValue
	private boolean required;	//是否是必传的，true则是必传。这个是 RequestParam(name=, value=, defaultValue=潍坊市, required=false) 这个required
	
	public ParamBean() {
		this.type = "String";
		this.required = false;
	}
	
	public String getName() {
		return FilterUtil.text(name);
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCommentText() {
		return FilterUtil.text(commentText);
	}
	public void setCommentText(String commentText) {
		if(commentText.indexOf("<required>") > -1) {
			this.required = true;
			commentText = FilterUtil.replaceAll(commentText, "<required>", "");
		}
		if(commentText.indexOf("<type=") > -1) {
			this.type = StringUtil.subString(commentText, "<type=", ">", 2);
        	commentText = FilterUtil.replaceAll(commentText, "<type="+this.type+">", "");
		}
		if(commentText.indexOf("<example=") > -1) {
			this.setDefaultValue(StringUtil.subString(commentText, "<example=", ">", 2));
            commentText = FilterUtil.replaceAll(commentText, "<example="+this.defaultValue+">", "");
		}
		this.commentText = commentText;
	}
	public String getType() {
		return type.toLowerCase();
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue.replaceAll("\"", "\\\\\"");
	}
	
	@Override
	public String toString() {
		return JSONObject.fromObject(this).toString();
	}
}
