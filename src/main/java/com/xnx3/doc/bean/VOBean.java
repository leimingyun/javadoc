package com.xnx3.doc.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * VO类
 * @author apple
 *
 */
public class VOBean {
	public List<VOParamBean> list;	//参数列表

	public VOBean() {
		this.list = new ArrayList<VOParamBean>();
	}
	
	public List<VOParamBean> getList() {
		return list;
	}

	public void setList(List<VOParamBean> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "VOBean [list=" + list + "]";
	}
	
}
