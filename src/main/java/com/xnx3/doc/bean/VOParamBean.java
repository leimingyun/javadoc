package com.xnx3.doc.bean;

import com.xnx3.doc.FilterUtil;

/**
 * VO中，具体参数的信息
 * @author apple
 *
 */
public class VOParamBean {
	private String name;	//参数名
	private String commentText;	//注释内容
	private String type;	//参数类型，取值有 String、int、Float 等
	private VOBean sub;		//子对象。如果当前参数类型type是一个对象，这里是这个参数对象的说明
	
	public String getName() {
		return FilterUtil.text(name);
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCommentText() {
		return FilterUtil.text(commentText);
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public VOBean getSub() {
		return sub;
	}
	public void setSub(VOBean sub) {
		this.sub = sub;
	}
	
}
