package com.xnx3.doc.bean;

import java.util.ArrayList;
import java.util.List;

import com.xnx3.doc.FilterUtil;

import net.sf.json.JSONObject;

/**
 * 类相关。也就是controller的信息存在于这里
 * @author 管雷鸣
 *
 */
public class ClassBean{
	private String className;	//类名
	private String commentText;	//注释内容
	private String urlPath;		//url的路径，如 /shop/api/
	private String author;	//作者
	private List<MethodBean> methodList;	//其内方法列表
	
	public ClassBean() {
		this.methodList = new ArrayList<MethodBean>();
	}
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getCommentText() {
		return FilterUtil.text(commentText);
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getUrlPath() {
		return urlPath;
	}
	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}
	public List<MethodBean> getMethodList() {
		return methodList;
	}
	public void setMethodList(List<MethodBean> methodList) {
		this.methodList = methodList;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return JSONObject.fromObject(this).toString();
	}
}
