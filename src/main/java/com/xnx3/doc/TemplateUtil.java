package com.xnx3.doc;

import java.io.IOException;
import com.xnx3.FileUtil;
import cn.zvo.http.Http;
import cn.zvo.http.Response;

/**
 * 模板
 * @author 管雷鸣
 *
 */
public class TemplateUtil {
//	private String templatePath;	//模板所在路径，如 http://res.zvo.cn/javadoc/template/ 这个目录下有 template.html、style.css、javadoc.js
	Http http; 
	private JavaDoc javadoc;	//用到里面的 templatePath、version、domain 等
	
	/**
	 * 传入模板所在文件夹的路径
	 * @param templatePath 可传入远程路径，如 http://res.zvo.cn/javadoc/template/ 或者本地路径如 /mnt/apple/download/template/
	 */
	public TemplateUtil(JavaDoc javadoc) {
//		this.templatePath = templatePath;
		this.javadoc = javadoc;
		this.http = new Http();
	}
	
	/**
	 * 判断 templatePath 这个路径是本地路径，还是远程url路径
	 * @return true 是本地路径；  false是远程url
	 */
	public boolean isLocal() {
		if(javadoc.templatePath == null) {
			return false;
		}
		if(javadoc.templatePath.indexOf("http://") == 0 || javadoc.templatePath.indexOf("https://") == 0) {
			return false;
		}else {
			return true;
		}
	}
	
	/**
	 * 获取 具体api接口的模板，也就是模板中的 template.html 模板
	 * @return
	 */
	public String getTemplate() {
		return getResource("template.html");
	}
	
	/**
	 * 获取 index.html 模板
	 * @return
	 */
	public String getIndex() {
		return getResource("index.html");
	}
	

	/**
	 * 获取 data.js 的模板
	 * @return
	 */
	public String getJavaDocJs() {
		return getResource("javadoc.js");
	}
	

	/**
	 * 获取 style.css 的模板
	 * @return
	 */
	public String getStyleCss() {
		return getResource("style.css");
	}
	
	/**
	 * 获取资源文件的源码，如 style。css、 template.html 等
	 * @param fileName 传入如 template.html
	 * @return 源码
	 */
	public String getResource(String fileName) {
		String source = "";
		if(isLocal()) {
			source = FileUtil.read(javadoc.templatePath+fileName, FileUtil.UTF8);
		}else {
			Response res = null;
			try {
				res = http.get(javadoc.templatePath+fileName);
			} catch (IOException e) {
				e.printStackTrace();
				return e.getMessage();
			}
			
			if(res.getCode() - 200 != 0) {
				Log.error("获取"+fileName+"："+res.toString());
				return "获取"+fileName+"异常："+res.toString();
			}
			
			source = res.getContent();
		}
		
		source = TemplateUtil.replaceAll(source, "\\{name\\}", javadoc.name);
		source = TemplateUtil.replaceAll(source, "\\{version\\}", javadoc.version);
		source = TemplateUtil.replaceAll(source, "\\{domain\\}", javadoc.domain);
		source = TemplateUtil.replaceAll(source, "\\{token\\}", javadoc.token);
		
		return source;
	}
	

	/**
	 * 替换特殊字符，避免再执行替换使，因为特殊字符的存在，影响正则匹配，导致替换出错
	 * @param sourceText 进行替换的原始字符串 sourceText.replaceALl
	 * @param regex 要替换sourceText的什么文字
	 * @param replacement 要将regex替换成什么
	 * @return 替换好的
	 */
	public static String replaceAll(String sourceText, String regex, String replacement){
		if(sourceText == null){
			return null;
		}
		if(regex == null || replacement == null){
			return sourceText;
		}

		//将$符号替换为 \$
		replacement = replacement.replaceAll("\\$", "\\\\\\$");  
		
		return sourceText.replaceAll(regex, replacement);
	}
	
}
