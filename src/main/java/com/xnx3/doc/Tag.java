package com.xnx3.doc;
/**
 * javadoc文档中使用的一些标签
 * @author 管雷鸣
 *
 */
public class Tag {
	
	/**
	 * 忽略某个类或者某个方法
	 * 可加入到类注释或方法注释上。
	 * 在某个方法的注释上加入此，代表生成接口文档时，会自动忽略这个方法。
	 * 在某个类的注释上加入此，代表生成接口文档时，会自动忽略当前这个类中的所有方法
	 */
	public static final String IGNORE = "<ignore>";
	
	
}
