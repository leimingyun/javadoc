package com.xnx3.doc;

/**
 * 过滤
 * @author 管雷鸣
 *
 */
public class FilterUtil {
	
	/**
	 * 对展示的自定义文本进行过滤，以保证能正常显示文本
	 * @param text 作者、备注、。。等可能有异常字符需要过滤的文本
	 * @return 过滤好的不会影响显示的文本
	 */
	public static String text(String text) {
		if(text == null) {
			return "";
		}
		if(text.length() == 0) {
			return text;
		}
		text = text.replaceAll("\"", "\\\\\"");
//		text = text.replaceAll("\\[required\\]", "");
		
		return text;
	}
	

	/**
	 * 替换特殊字符，避免再执行替换使，因为特殊字符的存在，影响正则匹配，导致替换出错
	 * @param sourceText 进行替换的原始字符串 sourceText.replaceALl
	 * @param regex 要替换sourceText的什么文字
	 * @param replacement 要将regex替换成什么
	 * @return 替换好的
	 */
	public static String replaceAll(String sourceText, String regex, String replacement){
		if(sourceText == null){
			return null;
		}
		if(regex == null || replacement == null){
			return sourceText;
		}

		//将$符号替换为 \$
		replacement = replacement.replaceAll("\\$", "\\\\\\$");  
		
		return sourceText.replaceAll(regex, replacement);
	}
	
}
